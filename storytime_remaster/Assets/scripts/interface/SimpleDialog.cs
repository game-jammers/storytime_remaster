//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Storytime
{
    public class SimpleDialog
        : UIElement
    {
        //
        // mebmers ////////////////////////////////////////////////////////////
        //

        [Header("UI Elements")]
        public AudioSource audioSource;
        public TextMeshProUGUI message;
        public Image image;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh(SceneManager.DialogEvent ev)
        {
            if(audioSource != null)
            {
                audioSource.clip = ev.soundFx;
                if(ev.soundFx != null)
                {
                    audioSource.Play();
                }
            }

            if(message != null)
            {
                message.text = ev.message;
            }

            if(image != null)
            {
                if(ev.image != null)
                {
                    image.gameObject.SetActive(true);
                    image.sprite = ev.image;
                }
                else
                {
                    image.gameObject.SetActive(false);
                }
            }
        }
    }
}

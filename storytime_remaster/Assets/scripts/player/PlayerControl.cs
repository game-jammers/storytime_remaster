//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace Storytime
{
    public class PlayerControl
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        private enum Action
        {
            LookX,
            LookY,
            MoveX,
            MoveY,
            Interact,
            Zoom
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Interface")]
        public Image reticle                                    = null;
        public Color defaultColor                               = Color.white;
        public Color interactColor                              = Color.red;

        [Header("Camera")]
        public Vector2 lookSpeed                                = new Vector2(90f, -90f);
        public Range<float> verticalRange                       = new Range<float>(-90f, 90f);
        public Range<float> zoomRange                           = new Range<float>(30f, 70f);
        [SerializeField] Vector2 lookAngles                     = Vector2.zero;

        [Header("Movement")]
        public bool canMove                                     = true;
        public UnityEngine.CharacterController unityController  { get; private set; }
        [ShowIf("canMove")] public float moveSpeed              = 10f;

        private InputRouter<Action> input                       = null;
        [SerializeField, ReadOnly] Interactable target          = null;

        private StorySceneManager manager                       = null;

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private Color GetReticleColor()
        {
            Color result = defaultColor;
            if(target != null)
                result = interactColor;

            return result;
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            manager = SceneManager.GetInstance<StorySceneManager>();
            unityController = GetComponent<UnityEngine.CharacterController>();
            input = new InputRouter<Action>();
            input.Bind(Action.LookX, InputAction.FromAxis(InputAction.Axis.MouseHorizontal, false, GameManager.instance.settings.mouse.speed.x));
            input.Bind(Action.LookY, InputAction.FromAxis(InputAction.Axis.MouseVertical, true, GameManager.instance.settings.mouse.speed.y));
            input.Bind(Action.MoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(Action.MoveY, InputAction.FromAxis(KeyCode.S, KeyCode.W));
            input.Bind(Action.Interact, InputAction.FromKey(KeyCode.Mouse0));
            input.Bind(Action.Zoom, InputAction.FromKey(KeyCode.Mouse1));
            if(canMove)
            {
                unityController = GetComponent<UnityEngine.CharacterController>();
                canMove = unityController != null;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void Update()
        {
            if(canMove)
            {
                Vector3 deltaMove = new Vector3(
                    input.GetAxis(Action.MoveX),
                    0f,
                    input.GetAxis(Action.MoveY)
                );

                deltaMove = SceneManager.instance.sceneCam.TransformDirection(deltaMove);
                deltaMove.y = 0f;

                deltaMove += Physics.gravity;

                unityController.Move(deltaMove * Time.deltaTime);
            }

            if(input.GetKeyPressed(Action.Interact) && target != null)
            {
                target.StartInteract();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void LateUpdate()
        {
            Vector2 deltaLook = new Vector2(
                input.GetAxis(Action.LookX) * lookSpeed.x,
                input.GetAxis(Action.LookY) * lookSpeed.y
            ) * Time.deltaTime;

            lookAngles.x = (lookAngles.x + deltaLook.x) % 360f;
            lookAngles.y = verticalRange.Clamp(lookAngles.y + deltaLook.y);

            transform.rotation = Quaternion.Euler(lookAngles.y, lookAngles.x, 0f);

            //
            // test for interactable
            //

            Interactable newTarget = SceneManager.instance.sceneCam.MousePick<Interactable>();
            if(target != null && target != newTarget)
            {
                target.FinishInteract();
            }
            target = newTarget;

            reticle.color = GetReticleColor();
            if(manager != null)
            {
                if(target == null)
                {
                    manager.reticleDisplay.text = System.String.Empty;
                }
                else
                {
                    manager.reticleDisplay.text = target.reticlePopup;
                }
            }
        }
    }
}

//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace Storytime
{
    public class Interactable
        : MonoBehaviour
    {
        //
        // types ////////////////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct InteractEvent
        {
            public SceneManager.DialogEvent dialog;
            public AudioClip sfx;
            public PlayableDirector[] begin;
            public PlayableDirector[] end;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public InteractEvent events;
        public AudioSource source;
        public string reticlePopup                              = System.String.Empty;
        public bool changeScene                                 = false;
        [ShowIf("changeScene")] public SceneId nextScene        = SceneId.Farmhouse;
        
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void StartInteract()
        {
            if(changeScene)
            {
                GameManager.ChangeScene(nextScene);
            }
            else
            {
                SceneManager.instance.dialogs.Show(events.dialog);
                if(events.sfx != null && source != null)
                {
                    source.clip = events.sfx;
                    source.Play();
                }

                foreach(PlayableDirector dir in events.begin)
                {
                    dir.Play();
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void FinishInteract()
        {
            SceneManager.instance.dialogs.Hide();
            foreach(PlayableDirector dir in events.end)
            {
                dir.Play();
            }
        }
    }
}

//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Storytime
{
    public class StoryManager
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        private class ScenesRaw
        {
            public SceneRaw[] scenes                            = null;
        }

        //
        // --------------------------------------------------------------------
        //
        

        [System.Serializable]
        private class SceneRaw
        {
            public int idx                                      = -1;
            public string id                                    = System.String.Empty;
            public string msg                                   = System.String.Empty;
            public int[] stems                                  = null;

            public Scene ToScene(StoryManager manager)
            {
                SceneId sceneId = (SceneId)System.Enum.Parse(typeof(SceneId), id);
                return new Scene() {
                    id = sceneId,
                    message = msg,
                    dialog = manager.LoadDialog(sceneId),
                    stems = manager.LoadStems(stems),
                };
            }
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public class Scene
        {
            public SceneId id;
            public string message;
            public AudioClip dialog;
            public AudioClip[] stems;
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public class DialogMap
            : EnumList<SceneId, AudioClip>
        {
        }

        //
        // ------------------------------------------------------------------------
        //
        
        [System.Serializable]
        public class SceneDataMap
            : EnumList<SceneId, Scene>
        {
        }

        //
        // members/////////////////////////////////////////////////////////////
        //

        [Header("Configuration")]
        [SerializeField] TextAsset storyJson                    = null;
        [SerializeField] DialogMap dialog                       = null;
        [SerializeField] AudioClip[] stems                      = null;

        [Header("Imported Data")]
        [SerializeField, ReadOnly] SceneDataMap scenes          = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public Scene GetScene(SceneId id)
        {
            return scenes[id];
        }

        //
        // --------------------------------------------------------------------
        //
        
        [ContextMenu("Reload")]
        public void Reload()
        {
            var rawScenes = new List<SceneRaw>();
            ScenesRaw loaded = JsonUtility.FromJson<ScenesRaw>(storyJson.text);
            if(loaded != null && loaded.scenes.Length > 0)
            {
                rawScenes.AddRange(loaded.scenes);
            }

            scenes = new SceneDataMap();
            for(int i = 0; i < rawScenes.Count; ++i)
            {
                Scene scene = rawScenes[i].ToScene(this);
                scenes[scene.id] = scene;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public AudioClip LoadDialog(SceneId id)
        {
            return dialog[id];
        }
        
        //
        // --------------------------------------------------------------------
        //

        public AudioClip[] LoadStems(int[] stemIds)
        {
            AudioClip[] result = new AudioClip[stemIds.Length];
            for(int i = 0; i < stemIds.Length; ++i)
            {
                int idx = stemIds[i]-1;
                Dbg.Assert(stems.IsValidIndex(idx), "Invalid Stem Requested {0}", idx);
                result[i] = stems[idx];
            }
            return result;
        }
        
    }
}

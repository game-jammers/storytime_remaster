//
// (c) GAME JAMMERS 2019
// http://jamming.games
//

using UnityEngine;

namespace Storytime
{
    [System.Serializable]
    public struct GameSettings
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        [System.Serializable]
        public struct Mouse
        {
            public CursorLockMode cursorMode;
            public bool visible;
            public Vector3 speed;
        };

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Mouse mouse;

        //
        // default ////////////////////////////////////////////////////////////
        //
        
        public static GameSettings Default
        {
            get
            {
                GameSettings result = new GameSettings();
                result.mouse.cursorMode = CursorLockMode.Locked;
                result.mouse.visible = true;
                result.mouse.speed = Vector3.one;

                return result;
            }
        }
    };
}

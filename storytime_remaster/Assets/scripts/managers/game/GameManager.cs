//
// (c) GAME JAMMERS 2019
// http://jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Storytime 
{
    public class GameManager
        : BaseGameManager
    {
        //
        // constants //////////////////////////////////////////////////////////
        //
        
        public static readonly string kPrefabPath               = "managers/GameManager";

        //
        // accessors ////////////////////////////////////////////////////////////
        //

        public static new GameManager instance                  { get; private set; }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public StoryManager storyManager                        = null;
        public GameSettings settings                            = GameSettings.Default;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static GameManager EnsureExists()
        {
            if( instance == null )
            {
                instance = EnsureExists<GameManager>( kPrefabPath );
                instance.name = "GameManager";
            }

            return instance;
        }

        //
        // --------------------------------------------------------------------
        //

        public static void ChangeScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Single );
        }

        //
        // --------------------------------------------------------------------
        //

        public static void AddScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Additive );
        }

        //
        // --------------------------------------------------------------------
        //

        public void ApplySettings()
        {
            ApplySettings(settings);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ApplySettings(GameSettings settings)
        {
            this.settings = settings;
            Cursor.lockState = settings.mouse.cursorMode;
            Cursor.visible = settings.mouse.visible;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void CaptureCursor()
        {
            settings.mouse.cursorMode = CursorLockMode.Locked;
            settings.mouse.visible = false;
            ApplySettings();
        }

        //
        // --------------------------------------------------------------------
        //

        public void ReleaseCursor()
        {
            settings.mouse.cursorMode = CursorLockMode.None;
            settings.mouse.visible = true;
            ApplySettings();
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected override void Awake()
        {
            base.Awake();
            ApplySettings();

            #if UNITY_EDITOR
                storyManager.Reload();
            #endif
        }
    };
}

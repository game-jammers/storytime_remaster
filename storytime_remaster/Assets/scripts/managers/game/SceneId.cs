//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;

namespace Storytime
{
    public enum SceneId
    {
        TitleScreen,
        Farmhouse,
        ClimbTheHill,
        ApproachMeteor,
        WatchDistance,
        GetYourCamera,
        RunForward,
        YellLoudly,
        FollowThem,
        TakePhotos,
        TakePhotosRoof,
        CoverEars,
        Listen,
        Flee,
        Stay,
        GrabChimney,
        TakePhoto,
        SeekHigher,
        SeekShelter,
        ListenRadio,
        GetOut,
        AnayaAcres,
        Dream,
        TurnBack
    }
}

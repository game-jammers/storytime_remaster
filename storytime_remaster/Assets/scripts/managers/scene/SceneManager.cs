//
// (c) GAME JAMMERS 2019
// http://jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Storytime
{
    public class SceneManager
        : BaseSceneManager
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum DialogStyle
        {
            NONE,
            DarkRPGBox,
            NotebookPaper,
            ZoomIn
        }

        //
        // --------------------------------------------------------------------
        //
        
        [System.Serializable]
        public struct DialogEvent
        {
            public DialogStyle style;
            public AudioClip soundFx;
            public Sprite image;
            [TextArea(5,8)] public string message;
        }

        //
        // --------------------------------------------------------------------
        //
        
        [System.Serializable]
        public class DialogBoxes
            : EnumList<DialogStyle, SimpleDialog>
        {
            //
            // members ////////////////////////////////////////////////////////
            //

            public UIElement root                               = null;
            
            //
            // public methods /////////////////////////////////////////////////
            //
            
            public void Show(DialogEvent ev, float fadeTime = 1f)
            {
                foreach(SimpleDialog iter in items)
                {
                    if(iter != null)
                    {
                        iter.Hide();
                    }
                }

                SimpleDialog dlg = this[ev.style];
                if(dlg != null)
                {
                    dlg.Refresh(ev);
                    dlg.Show();

                    root.Show(fadeTime, null);
                }
            }

            //
            // ----------------------------------------------------------------
            //
            
            public void Hide(float fadeTime = 1f)
            {
                root.Hide(fadeTime, null);
            }
        }
        
        //
        // events //////////////////////////////////////////////////////////////
        //

        public delegate void SceneReadyCallback();
        public event SceneReadyCallback OnSceneReady;

        //
        // members /////////////////////////////////////////////////////////////
        //

        public static new SceneManager instance                 { get; private set; }

        [Header("Dialogs")]
        public UIElement dialogRoot;
        public DialogBoxes dialogs;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Awake()
        {
            base.Awake();
            instance = this;
            GameManager.EnsureExists();

            dialogs.root = dialogRoot;
            dialogs.Hide(0f);
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected void NotifySceneReady()
        {
            if( OnSceneReady != null )
            {
                OnSceneReady();
            }
        }
    }
}

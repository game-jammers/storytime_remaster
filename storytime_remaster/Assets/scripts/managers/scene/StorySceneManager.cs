//
// (c) GameJammers 2020
// http://www.jamming.games
//

using Cinemachine;
using TMPro;
using blacktriangles;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

namespace Storytime
{
    public class StorySceneManager
        : SceneManager
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        private enum SceneType
        {
            Original,
            Remaster
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Story Settings")]
        public SceneId sceneId                                  = SceneId.Farmhouse;
        [SerializeField] SceneType sceneType                    = SceneType.Remaster;

        [Header("References")]
        AudioSource dialog                                      = null;
        AudioSource[] stems                                     = null;
        [SerializeField] TextMeshProUGUI message                = null;
        public TextMeshProUGUI reticleDisplay                   = null;
        [SerializeField] AudioSource dialogSourcePrefab         = null;
        [SerializeField] AudioSource stemSourcePrefab           = null;

        [Header("Original Scene")]
        [SerializeField] GameObject originalRoot                = null;
        //[SerializeField] CinemachineVirtualCameraBase originalCam= null;

        [Header("Remaster Scene")]
        [SerializeField] GameObject remasterRoot                = null;
        //[SerializeField] CinemachineVirtualCameraBase remasterCam= null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        [ContextMenu("Cycle Scene Type")]
        public void CycleSceneType()
        {
            sceneType = (SceneType)(((int)sceneType + 1) % EnumUtility.Count<SceneType>());
            RefreshScene();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        [ContextMenu("Play Story")]
        private void PlayStory()
        {
            StoryManager.Scene scene = GameManager.instance.storyManager.GetScene(sceneId);

            // text
            message.text = scene.message;

            // dialog
            if(dialog != null)
            {
                Destroy(dialog.gameObject);
            }

            dialog = Instantiate(dialogSourcePrefab);
            dialog.clip = scene.dialog;
            dialog.Play();

            // stems
            if(stems != null && stems.Length > 0)
            {
                foreach(AudioSource stem in stems)
                {
                    Destroy(stem.gameObject);
                }
            }

            stems = new AudioSource[scene.stems.Length];
            for(int i = 0; i < scene.stems.Length; ++i)
            {
                AudioSource ac = Instantiate(stemSourcePrefab) as AudioSource;
                ac.clip = scene.stems[i];
                ac.Play();
                stems[i] = ac;
            }
        }

        [ContextMenu("Refresh Scene")]
        private void RefreshScene()
        {
            originalRoot.SetActive(sceneType == SceneType.Original);
            remasterRoot.SetActive(sceneType == SceneType.Remaster);
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Start()
        {
            RefreshScene();
            PlayStory();
        }
    }
}
